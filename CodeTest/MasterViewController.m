//
//  MasterViewController.m
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AlbumListCustomTableViewCell.h"
#import "AlbumModel.h"

@interface MasterViewController ()

@property NSMutableArray *albumEntityArray;


//UI Components
@property (strong, nonatomic) IBOutlet UITableView *albumListTableView;
@property (strong, nonatomic) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UITextField *nameTxtFld;

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
       
    //Create Search Bar
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    //self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.placeholder=SEARCH_PLACEHOLDER;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate =self;
    self.albumListTableView.tableHeaderView = self.searchController.searchBar;

    
    //Create detailed view
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.searchController.active = YES;
    });

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.searchController.active = NO;
    });

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - updateUI

- (void)updateUI:(NSArray*)dataArray
{
    
    if(self.albumEntityArray)
    {
        [self.albumEntityArray removeAllObjects];
    }
    else{
        
        self.albumEntityArray=[[NSMutableArray alloc]initWithCapacity:0];
    }
    
    for (NSDictionary *dict in dataArray) {
        
        AlbumModel *model=[[AlbumModel alloc]init];
        model.trackName=[dict valueForKey:@"trackName"];
        model.artistName=[dict valueForKey:@"artistName"];
        model.collectionPrice=[[dict valueForKey:@"trackPrice"] stringValue];
        model.releaseDate=[dict valueForKey:@"releaseDate"];
        model.artworkUrl100=[dict valueForKey:@"artworkUrl100"];
        
        [self.albumEntityArray addObject:model];
    }
    
   // self.albumEntityArray=dataArray;
    
    [self.albumListTableView reloadData];

    
}

#pragma mark - SearchBar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [super sendUserSearchText:searchBar.text];
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.albumListTableView indexPathForSelectedRow];
        AlbumModel *model = self.albumEntityArray[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:model];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.albumEntityArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumListCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
      [cell configureCellWithData:[self.albumEntityArray objectAtIndex:indexPath.row]];
    
     return cell;
}



@end
