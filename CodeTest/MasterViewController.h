//
//  MasterViewController.h
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@class DetailViewController;

@interface  MasterViewController :BaseViewController  <UISearchBarDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

