//
//  AlbumModel.h
//  CodeTest
//
//  Created by Sahil Bhardwaj on 29/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlbumModel : NSObject

@property (nonatomic, strong) NSString *trackName;
@property (nonatomic, strong) NSString *artistName;
@property (nonatomic, strong) NSString *releaseDate;
@property (nonatomic, strong) NSString *collectionPrice;
@property (nonatomic,strong)  NSString *artworkUrl100;

@end
