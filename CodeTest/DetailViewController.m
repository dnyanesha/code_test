//
//  DetailViewController.m
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DetailViewController.h"


@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *circularIamge;
@property (weak, nonatomic) IBOutlet UILabel *albumName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *artistName;
@property (weak, nonatomic) IBOutlet UILabel *releaseDate;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
       
        // Update the view.
       // [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        
        
        // TODO : Null check for dic values
        /*
        if ([_detailItem objectForKey:@"trackName"] == [NSNull null]) {
           // Display the alert
        }
         */
        
        
        
        //cell.imageView.image = nil;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            //  TODO: cache this explicitly instead of reloading every time.
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_detailItem.artworkUrl100]];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                _circularIamge.image = image;
              
                //TODO: a Utility can be created for this if need to create cricular view anywhere else as well 
                _circularIamge.layer.cornerRadius = _circularIamge.frame.size.height/2;
                _circularIamge.clipsToBounds = YES;
                _circularIamge.layer.borderColor = [UIColor whiteColor].CGColor;
                _circularIamge.layer.borderWidth=2;
                [self.view setNeedsLayout];
            });
        });

        
        
        _albumName.text=_detailItem.trackName;
        _artistName.text=_detailItem.artistName;
        _price.text=_detailItem.collectionPrice;
        _releaseDate.text=_detailItem.releaseDate;
        
        
        //TODO : We can arrabge views by usning stack view but it will not work for ios 8
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
