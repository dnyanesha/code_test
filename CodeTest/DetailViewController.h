//
//  DetailViewController.h
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumModel.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) AlbumModel *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

