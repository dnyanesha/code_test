//
//  AlbumListCustomTableViewCell.m
//  CodeTest
//
//  Created by Dnyanesh Ayachit on 27/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AlbumListCustomTableViewCell.h"
#import "AlbumModel.h"

@implementation AlbumListCustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithData:(AlbumModel*)albumDataEntity
{
    self.artistName.text = albumDataEntity.artistName;
    self.trackName.text = albumDataEntity.trackName;
    
        
     if (albumDataEntity)
     {
     //cell.imageView.image = nil;
     dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
     dispatch_async(queue, ^(void) {
     
     //  TODO: cache this explicitly instead of reloading every time.
     NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:albumDataEntity.artworkUrl100]];
     UIImage* image = [[UIImage alloc] initWithData:imageData];
     dispatch_async(dispatch_get_main_queue(), ^{
     self.AlbumImage.image = image;
     [self setNeedsLayout];
     });
     });
     
     }
    
}

@end
