//
//  BaseViewController.m
//  CodeTest
//
//  Created by Dnyanesh Ayachit  on 27/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property NSArray *entityArray;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendUserSearchText:(NSString *)searchBarText
{
    //Clearing previous results from array
    _entityArray=nil;
    
    NSLog(@"my search text is =%@", searchBarText);
    
    
    //Network connection
    NetworkSession *session = [NetworkSession sharedManager];
    NSURL *JSONURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ROOT_URL,[searchBarText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]]];
    
    //TODO : If application follows some particualr POST and GET request format we can use Methods in Utility classes and send Request instead of JSON URL
    [session NetworkSessionwithRequest:JSONURL];
    
    
    session.errorHandler = ^(NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self presentViewController:[Utility DisplayErrorMessage:error] animated:YES completion:nil];
        });
    };
    
    session.successData = ^(NSData *data){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.queue = [[NSOperationQueue alloc] init];
            _parser = [[ParseOperation alloc] initWithData:data];
            __weak BaseViewController *weakSelf = self;
            
            self.parser.parseError = ^(NSError *parseError) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [weakSelf presentViewController:[Utility DisplayErrorMessage:parseError] animated:YES completion:nil];
                });
            };
            
            __weak ParseOperation *weakParser = self.parser;
            
            
            self.parser.completionBlock = ^(void) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (weakParser.entityArray != nil)
                {
                    // TODO: We can create a array of model class and use it instead of direct array
                    weakSelf.entityArray = weakParser.entityArray;
                    
                    NSLog(@"data array is  =%@", weakSelf.entityArray);
                    
                    // The completion block may execute on any thread.  Because operations
                    // involving the UI are about to be performed, make sure they execute on the main thread.
                    
                    
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf updateUI: weakSelf.entityArray];
                            
                        });
                    
                    
                    
                }
                
                // we are finished with the queue and our ParseOperation
                weakSelf.queue = nil;
            };
            [self.queue addOperation:self.parser];
        });
    };
    
}

// This  method will update UI of respective child classes 
- (void)updateUI:(NSArray*)dataArray
{
    NSLog(@"Implmentation in child classes");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
