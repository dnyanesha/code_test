//
//  BaseViewController.h
//  CodeTest
//
//  Created by Dnyanesh Ayachit  on 27/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "NetworkSession.h"
#import "Utility.h"
#import "ParseOperation.h"

@interface BaseViewController : UIViewController


@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) ParseOperation *parser;


- (void)sendUserSearchText:(NSString *)searchBarText;
- (void)updateUI:(NSArray*)dataArray;
@end
