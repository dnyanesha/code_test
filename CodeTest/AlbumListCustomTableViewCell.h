//
//  AlbumListCustomTableViewCell.h
//  CodeTest
//
//  Created by Dnyanesh Ayachit on 27/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumListCustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *AlbumImage;
@property (weak, nonatomic) IBOutlet UILabel *artistName;
@property (weak, nonatomic) IBOutlet UILabel *trackName;

-(void)configureCellWithData:(NSDictionary*)albumDataEntity;

@end
