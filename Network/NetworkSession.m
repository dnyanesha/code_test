//
//  NetworkSession.m
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "NetworkSession.h"
@import UIKit;

@interface NetworkSession() <NSURLSessionTaskDelegate>

@end
@implementation NetworkSession


//Siglton Intance
+ (id)sharedManager
{
    static NetworkSession *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


//TODO : Instead of URL we can pass request GET/POST
-(void)NetworkSessionwithRequest:(NSURL*)JSONURL
{
    
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:JSONURL];
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
       
        if (error != nil)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock: ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                if ([error code] == NSURLErrorAppTransportSecurityRequiresSecureConnection)
                {
                    abort();
                }
                else
                {
                    if (self.errorHandler){
                        self.errorHandler(error);
                    }
                }
            }];
        }
        else{
            if (self.successData) {
                self.successData(data);
            }
        }
    }];
    
    [dataTask resume];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

}

#pragma mark - NSUrlSession with delegate

-(void)NetworkSessionRequestWithDelegate:(NSMutableURLRequest *)request
{
    //TODO: Can use delegate methods if needed in some cases
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    //TODO: Can use delegate methods if needed in some cases
}

    @end

