//
//  NetworkSession.h
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkSession : NSObject

////GCD
@property (nonatomic, copy) void (^errorHandler)(NSError *error);
@property (nonatomic, copy) void (^successData)(NSData *data);
@property (nonatomic, copy) void (^getOperationStatus)(NSString *operationStatus);

//Methods
+ (id)sharedManager;
-(void)NetworkSessionwithRequest:(NSURL*)JSONURL;
-(void)NetworkSessionRequestWithDelegate:(NSMutableURLRequest *)request;

@end
