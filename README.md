# README #

**About Project :** 

The project contains list of albums and their detail.
1. Used standard JSON parser to parse data
2.Used Asynchronous image downloading
3.Used GCD & operations where necessary 


 **Cool things could have done :**

- Create Model classes
- Create Database for data and save images in cache 
- Instead of directly fetching data in viewcontroller we can introduce a business layer where we can manipulate data / put validations etc.
- There is scope for UI design improvements