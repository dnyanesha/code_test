//
//  ParseOperation.m
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.


#import "ParseOperation.h"

@interface ParseOperation()

@property (nonatomic, strong) NSData *dataToParse;
@property (nonatomic, strong) NSArray *entityArray;
@property (nonatomic, strong) NSDictionary *entityDictonary;
@property (nonatomic, strong) NSMutableArray *workingArray;
@property (nonatomic, strong) NSMutableDictionary *workingDictonary;

@end

@implementation ParseOperation

- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self != nil)
    {
        _dataToParse = data;
    }
    return self;
}

#pragma Main - Entry point of operation

-(void)main
{
    NSError *error = nil;
    
    _workingArray = [NSMutableArray array];
    
    id json = ([NSJSONSerialization JSONObjectWithData: _dataToParse options: NSJSONReadingMutableContainers error: &error]);
    
    if (!json) {
        if (self.parseError){
            self.parseError(error);
        }
        }else{
            
            if ([json isKindOfClass:[NSArray class]]) {
                for(NSDictionary *item in (NSArray*)json) {
                    //TODO in case we have array from JSON
                    NSLog(@"Item: %@", item);
//                    [self.workingArray addObject:[item valueForKey:@"phrase"]];
                }
            }else if ([json isKindOfClass:[NSDictionary class]]){
                _workingDictonary = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
                [self.workingArray addObjectsFromArray:[_workingDictonary valueForKey:@"results"]];
            }
        }
    
    if (![self isCancelled])
    {
        // Set verification phrases to the result of our parsing
        if ([json isKindOfClass:[NSArray class]]) {
           // self.entityArray = [NSArray arrayWithArray:self.workingArray];
        }else if ([json isKindOfClass:[NSDictionary class]]){
            //Todo
            //self.entityDictonary = [NSDictionary dictionaryWithDictionary:self.workingDictonary];
            
            self.entityArray = self.workingArray;
        }
        
    }
    self.workingDictonary = nil;
    self.workingArray = nil;
    self.dataToParse = nil;

}

@end
