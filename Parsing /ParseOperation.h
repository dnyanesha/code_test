//
//  ParseOperation.h
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.

#import <Foundation/Foundation.h>

@interface ParseOperation : NSOperation

//GCD
@property (nonatomic, copy) void (^parseError)(NSError *error);

//Collections
@property (nonatomic, strong, readonly) NSArray *entityArray;
@property (nonatomic, strong, readonly) NSDictionary *entityDictonary;

//Methods
- (instancetype)initWithData:(NSData *)data;

@end
