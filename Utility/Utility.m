//
//  Utility.m
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.
#import "Utility.h"
@import UIKit;

@implementation Utility

+(NSMutableURLRequest*)createGETRequestForNetworkSession:(NSString*)uri
{
    NSMutableURLRequest* _request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:uri]];
    [_request setHTTPMethod:@"GET"];
    // Request headers
    [_request setValue:@"SOME_VALUE" forHTTPHeaderField:@"SOME_KEY"];
    // Request body
    [_request setHTTPBody:[@"{body}" dataUsingEncoding:NSUTF8StringEncoding]];
    
    return _request;
}

+(NSMutableURLRequest*)createPOSTRequestForNetworkSession:(NSString*)uri paramDict:(NSArray*)param
{
    NSMutableURLRequest* _request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:uri]];
    [_request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [_request setHTTPMethod:@"POST"];
    // Request headers
    [_request setValue:@"SOME_VALUE"forHTTPHeaderField:@"SOME_KEY"];
    
    // Request body
    if (param) {
        for (NSString *str in param) {
            [_request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }else
        [_request setHTTPBody:[@"{body}" dataUsingEncoding:NSUTF8StringEncoding]];
    
    return _request;
}

// TODO: Instead of creating Alerts multiple times we can create "AlertHelperViewController"

+ (UIAlertController*)DisplayErrorMessage:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cannot Show data"
                                                                   message:errorMessage
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         // dissmissal of alert completed
                                                     }];
    
    [alert addAction:OKAction];
    return alert;
    
}

+ (UIAlertController*)DisplayMessage:(NSString *)errorMessage title:(NSString*)title
{
    
  
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:errorMessage
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         // dissmissal of alert completed
                                                     }];
    
    [alert addAction:OKAction];
    return alert;
    
}

@end
