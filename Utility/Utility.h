//
//  Utility.h
//  CodeTest
//
//  Created by Admin on 10/11/16.
//  Copyright © 2016 Admin. All rights reserved.

#import <Foundation/Foundation.h>
@import UIKit;
@interface Utility : NSObject

+(NSMutableURLRequest*)createGETRequestForNetworkSession:(NSString*)uri; 
+(NSMutableURLRequest*)createPOSTRequestForNetworkSession:(NSString*)uri paramDict:(NSArray*)param;
+ (UIAlertController*)DisplayErrorMessage:(NSError *)error;
+ (UIAlertController*)DisplayMessage:(NSString *)errorMessage title:(NSString*)title;
@end
